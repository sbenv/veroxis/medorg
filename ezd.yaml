# yaml-language-server: $schema=https://gitlab.com/sbenv/veroxis/ezd-rs/-/raw/master/ezd.schema.json
---
tasks:
  shell_exec:
    check:
      description: check everything
      commands:
        - ezd run check:fmt
        - ezd run check:clippy
        - ezd run check:test
    fix:
      description: autofix with all linters
      commands:
        - ezd run fix:fmt
        - ezd run fix:clippy
    clean:
      description: delete build artifacts
      commands: [ezd run rust:1.66.0-x86_64-unknown-linux-musl -- cargo clean]
    check:fmt:
      description: autofix with linter `cargo fmt`
      commands: [ezd run rust:1.66.0-x86_64-unknown-linux-musl -- cargo +nightly fmt -- --check]
    fix:fmt:
      description: autofix with linter `cargo fmt`
      commands: [ezd run rust:1.66.0-x86_64-unknown-linux-musl -- cargo +nightly fmt]
    check:clippy:
      description: autofix with linter `cargo clippy`
      commands: [ezd run rust:1.66.0-x86_64-unknown-linux-musl -- cargo clippy --all-targets --all-features -- -D warnings]
    fix:clippy:
      description: autofix with linter `cargo clippy`
      commands: [ezd run rust:1.66.0-x86_64-unknown-linux-musl -- cargo clippy --fix --allow-dirty --allow-staged --all-targets --all-features -- -D warnings]
    check:test:
      description: autofix with linter `cargo clippy`
      commands: [ezd run rust:1.66.0-x86_64-unknown-linux-musl -- cargo-zigbuild test --target=x86_64-unknown-linux-musl]
    build:debug:
      description: build the application in `debug` mode
      commands: [ezd run build:x86_64-unknown-linux-musl:debug]
    build:release:
      description: build the application in `release` mode
      commands: [ezd run build:x86_64-unknown-linux-musl:release]
    build:all:release:
      description: build the application in `release` mode
      commands:
        - ezd run build:x86_64-unknown-linux-musl:release
        - ezd run build:aarch64-unknown-linux-musl:release
        - ezd run build:arm-unknown-linux-musleabihf:release
    build:x86_64-unknown-linux-musl:debug:
      description: build the application in `debug` mode
      commands: [ezd run rust:1.66.0-x86_64-unknown-linux-musl -- "cargo zigbuild --target=x86_64-unknown-linux-musl"]
    build:x86_64-unknown-linux-musl:release:
      description: build the application in `release` mode
      commands:
        - ezd run rust:1.66.0-x86_64-unknown-linux-musl -- "cargo +nightly zigbuild -Z build-std=std,panic_abort -Z build-std-features=panic_immediate_abort --target=x86_64-unknown-linux-musl --release"
    build:aarch64-unknown-linux-musl:debug:
      description: build the application in `debug` mode
      commands: [ezd run rust:1.66.0-aarch64-unknown-linux-musl -- "cargo zigbuild --target=aarch64-unknown-linux-musl"]
    build:aarch64-unknown-linux-musl:release:
      description: build the application in `release` mode
      commands:
        - ezd run rust:1.66.0-aarch64-unknown-linux-musl -- "cargo +nightly zigbuild -Z build-std=std,panic_abort -Z build-std-features=panic_immediate_abort --target=aarch64-unknown-linux-musl --release"
    build:arm-unknown-linux-musleabihf:debug:
      description: build the application in `debug` mode
      commands: [ezd run rust:1.66.0-arm-unknown-linux-musleabihf -- cargo zigbuild --target=arm-unknown-linux-musleabihf]
    build:arm-unknown-linux-musleabihf:release:
      description: build the application in `release` mode
      commands:
        - ezd run rust:1.66.0-arm-unknown-linux-musleabihf -- "cargo +nightly zigbuild -Z build-std=std,panic_abort -Z build-std-features=panic_immediate_abort --target=arm-unknown-linux-musleabihf --release"
  docker_seq:
    rust:1.66.0-x86_64-unknown-linux-musl:
      description: execute commands within a builder container
      config:
        image: registry.gitlab.com/sbenv/veroxis/images/sdks/rust:1.80.1-x86_64-unknown-linux-musl
        volumes:
          - .:/app
          - cache_medorg:/usr/local/cargo/registry
        workdir: /app
        sequence:
          - user: 0:0
            commands:
              - useradd --create-home --uid "__EZD_HOST_UID__" "__EZD_HOST_USERNAME__"
              - chown -R "__EZD_HOST_UID__:__EZD_HOST_GID__" "/usr/local/cargo/registry"
          - local_user: true
            commands: [set -eux && $EZD_FORWARDED_ARGS]
    rust:1.66.0-aarch64-unknown-linux-musl:
      description: execute commands within a builder container
      config:
        image: registry.gitlab.com/sbenv/veroxis/images/sdks/rust:1.80.1-aarch64-unknown-linux-musl
        volumes:
          - .:/app
          - cache_medorg:/usr/local/cargo/registry
        workdir: /app
        sequence:
          - user: 0:0
            commands:
              - useradd --create-home --uid "__EZD_HOST_UID__" "__EZD_HOST_USERNAME__"
              - chown -R "__EZD_HOST_UID__:__EZD_HOST_GID__" "/usr/local/cargo/registry"
          - local_user: true
            commands: [set -eux && $EZD_FORWARDED_ARGS]
    rust:1.66.0-arm-unknown-linux-musleabihf:
      description: execute commands within a builder container
      config:
        image: registry.gitlab.com/sbenv/veroxis/images/sdks/rust:1.80.1-arm-unknown-linux-musleabihf
        volumes:
          - .:/app
          - cache_medorg:/usr/local/cargo/registry
        workdir: /app
        sequence:
          - user: 0:0
            commands:
              - useradd --create-home --uid "__EZD_HOST_UID__" "__EZD_HOST_USERNAME__"
              - chown -R "__EZD_HOST_UID__:__EZD_HOST_GID__" "/usr/local/cargo/registry"
          - local_user: true
            commands: [set -eux && $EZD_FORWARDED_ARGS]

volumes:
  cache_medorg:
