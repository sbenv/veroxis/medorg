use std::path::PathBuf;

use filestats::file_hash_from_path;
use filestats::FileCategory;
use filestats::FileInfo;
use fs_crawler::files;
use futures::future;
use tokio::task;

pub fn create_output_directories(root: PathBuf) {
    let root = root.to_string_lossy();
    let videos: &str = FileCategory::Video.into();
    let videos = format!("{root}/{videos}");
    std::fs::create_dir_all(videos).expect("failed to create videos dir");
    let images: &str = FileCategory::Image.into();
    let images = format!("{root}/{images}");
    std::fs::create_dir_all(images).expect("failed to create images dir");
    let archives: &str = FileCategory::Archive.into();
    let archives = format!("{root}/{archives}");
    std::fs::create_dir_all(archives).expect("failed to create archives dir");
    let documents: &str = FileCategory::Document.into();
    let documents = format!("{root}/{documents}");
    std::fs::create_dir_all(documents).expect("failed to create documents dir");
    let other: &str = FileCategory::Other.into();
    let other = format!("{root}/{other}");
    std::fs::create_dir_all(other).expect("failed to create other dir");
}

pub async fn run(delete_source: bool, dry_run: bool, source_dirs: Vec<PathBuf>, out_dir: PathBuf) {
    let mut handles = vec![];
    for source_dir in source_dirs {
        let source_dir_clone = source_dir.clone();
        let out_dir_clone = out_dir.clone();
        handles.push(task::spawn(async move {
            handle_source_dir(delete_source, dry_run, source_dir_clone, out_dir_clone).await;
        }));
    }
    let _ = future::join_all(handles).await;
}

async fn handle_source_dir(
    delete_source: bool,
    dry_run: bool,
    source_dir: PathBuf,
    out_dir: PathBuf,
) {
    let dirs = fs_crawler::find_dirs_recursive(source_dir.to_owned());
    let mut handles = vec![];
    for dir in dirs.iter() {
        let dir_clone = dir.clone();
        let out_dir_clone = out_dir.clone();
        handles.push(task::spawn(async move {
            handle_media_dir(delete_source, dry_run, dir_clone, out_dir_clone).await;
        }));
    }
    let _ = future::join_all(handles).await;
}

async fn handle_media_dir(
    delete_source: bool,
    dry_run: bool,
    media_dir: PathBuf,
    out_dir: PathBuf,
) {
    let files = files(media_dir.as_path());
    let mut handles = vec![];
    for file in files.iter() {
        let file_clone = file.clone();
        let out_dir_clone = out_dir.clone();
        handles.push(task::spawn(async move {
            handle_media_file(delete_source, dry_run, file_clone, out_dir_clone).await;
        }));
    }
    let _ = future::join_all(handles).await;
}

async fn handle_media_file(
    delete_source: bool,
    dry_run: bool,
    media_file: PathBuf,
    out_dir: PathBuf,
) {
    // detect file infos
    if let Ok(file_info) = FileInfo::try_from(media_file.as_path()) {
        // calculate file hash
        // copy file if target doesnt exist (os file locking?)
        // delete source, if requested
        use filestats::FileCategory::*;
        match file_info.category {
            Unknown => {
                tracing::info!("Skipped: {:?}", file_info);
            }
            Image | Video | Archive | Document | Other => {
                // calculate target path:
                let hash = file_info.hash().await.unwrap();
                let file_type: Option<&str> = file_info.r#type.into();
                let file_type = file_type.unwrap();
                let category: &str = file_info.category.into();
                let out_dir = out_dir.to_string_lossy();
                let out_path_string = format!("{out_dir}/{category}/{hash}.{file_type}");
                let out_path = PathBuf::from(&out_path_string);
                let source_path_string = file_info.path.to_string_lossy();
                if out_path.exists() {
                    let target_hash = file_hash_from_path(&out_path).unwrap();
                    match target_hash == hash {
                        true => {
                            tracing::debug!("already exists: {out_path_string}");
                        }
                        false => {
                            tracing::debug!("overwrite: {out_path_string}");
                            if !dry_run {
                                tokio::fs::copy(&file_info.path, out_path).await.unwrap();
                            }
                        }
                    }
                } else {
                    tracing::debug!("create file: {out_path_string}");
                    if !dry_run {
                        tokio::fs::copy(&file_info.path, out_path).await.unwrap();
                    }
                }
                if delete_source {
                    tracing::debug!("delete file: {source_path_string}");
                    if !dry_run {
                        tokio::fs::remove_file(&file_info.path).await.unwrap();
                    }
                }
            }
        }
    }
}
