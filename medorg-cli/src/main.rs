use std::path::PathBuf;

use clap::*;
use medorg_core::create_output_directories;

#[derive(Debug, Parser)]
#[clap()]
pub struct CliArgs {
    /// delete media source files after being copied
    #[clap(long, value_parser)]
    pub delete_source: bool,

    /// dry run
    #[clap(long, value_parser)]
    pub dry_run: bool,

    /// source directories
    #[clap(short, long, value_parser)]
    pub source_dirs: Vec<PathBuf>,

    /// output directory
    #[clap(short, long, value_parser)]
    pub output_dir: PathBuf,
}

#[tokio::main]
async fn main() {
    init_logging();
    let args = CliArgs::parse();
    dbg!(&args);
    let mut source_dirs = vec![];
    for dir in args.source_dirs {
        if !dir.exists() {
            tracing::error!("source directory does not exist: {dir:?}");
            continue;
        }
        source_dirs.push(dir.canonicalize().expect("can't canonicalize source_dir"))
    }
    if !args.output_dir.exists() {
        std::fs::create_dir_all(&args.output_dir).expect("failed to create output_dir");
    }
    let output_dir = args
        .output_dir
        .canonicalize()
        .expect("can't canonicalize output_dir");
    create_output_directories(output_dir.clone());
    medorg_core::run(args.delete_source, args.dry_run, source_dirs, output_dir).await;
}

fn init_logging() {
    #[cfg(debug_assertions)]
    {
        if std::env::var("RUST_LIB_BACKTRACE").is_err() {
            std::env::set_var("RUST_LIB_BACKTRACE", "1")
        }
        if std::env::var("RUST_BACKTRACE").is_err() {
            std::env::set_var("RUST_BACKTRACE", "full")
        }
    }

    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "info")
    }

    tracing_subscriber::fmt::init();
}
