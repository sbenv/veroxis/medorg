use std::path::Path;
use std::path::PathBuf;

pub fn find_dirs_recursive(base_path: PathBuf) -> Vec<PathBuf> {
    let mut buff = vec![];
    find_dirs_intern(&mut buff, base_path);
    buff.sort();
    buff.dedup();
    buff
}

fn find_dirs_intern(buff: &mut Vec<PathBuf>, mut current_path: PathBuf) {
    if current_path.is_symlink() {
        current_path = current_path.read_link().unwrap();
        if buff.contains(&current_path) {
            return;
        }
    }
    if let Ok(current_path) = current_path.canonicalize() {
        if current_path.is_dir() {
            if buff.contains(&current_path) {
                return;
            }
            buff.push(current_path.to_owned());
            if let Ok(entries) = current_path.read_dir() {
                for entry in entries.flatten() {
                    let path = entry.path();
                    if path.is_dir() {
                        if buff.contains(&path) {
                            continue;
                        }
                        let dirs = find_dirs_recursive(path);
                        for dir in dirs {
                            buff.push(dir.to_owned());
                        }
                    }
                }
            }
        }
    }
}

pub fn files(base_path: &Path) -> Vec<PathBuf> {
    let mut buff = vec![];
    for entry in base_path.read_dir().expect("was not a directory").flatten() {
        let path = entry.path();
        if path.is_file() {
            buff.push(path);
        }
    }
    buff
}
