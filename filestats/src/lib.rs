use std::path::Path;
use std::path::PathBuf;

use thiserror::Error;

#[derive(Debug, Error)]
pub enum FileInfoError {
    #[error("PathDoesNotExist: `{0}`")]
    PathDoesNotExist(PathBuf),
    #[error("PathIsNotAFile: `{0}`")]
    PathIsNotAFile(PathBuf),
    #[error("FileExtensionUnknown: `{0}`")]
    FileExtensionUnknown(String),
    #[error("IoError: `{0}`")]
    IoError(#[from] std::io::Error),
}

#[derive(Debug, Default)]
pub struct FileInfo {
    pub path: PathBuf,
    pub r#type: FileType,
    pub category: FileCategory,
}

#[derive(Debug, PartialEq)]
pub enum FileType {
    Asf,
    Avi,
    Csv,
    Db,
    Flv,
    Gif,
    Html,
    Jfif,
    Jpg,
    Json,
    M4v,
    Markdown,
    Mkv,
    Mov,
    Mp4,
    Mpg,
    Odt,
    Pdf,
    Php,
    Png,
    Rar,
    Rlib,
    Rm,
    Rmvb,
    Sevenzip,
    Svg,
    Text,
    Toml,
    Torrent,
    Unknown(Option<String>),
    Vid,
    Vtx,
    Webm,
    Webp,
    Wmv,
    Xcf,
    Yaml,
    Zip,
}

impl Default for FileType {
    fn default() -> FileType {
        FileType::Unknown(None)
    }
}

impl From<FileType> for Option<&str> {
    fn from(file_type: FileType) -> Self {
        match file_type {
            FileType::Asf => Some("asf"),
            FileType::Avi => Some("avi"),
            FileType::Csv => Some("csv"),
            FileType::Db => Some("db"),
            FileType::Flv => Some("flv"),
            FileType::Gif => Some("gif"),
            FileType::Html => Some("html"),
            FileType::Jfif => Some("jfif"),
            FileType::Jpg => Some("jpg"),
            FileType::Json => Some("json"),
            FileType::M4v => Some("m4v"),
            FileType::Markdown => Some("md"),
            FileType::Mkv => Some("mkv"),
            FileType::Mov => Some("mov"),
            FileType::Mp4 => Some("mp4"),
            FileType::Mpg => Some("mpg"),
            FileType::Odt => Some("odt"),
            FileType::Pdf => Some("pdf"),
            FileType::Php => Some("php"),
            FileType::Png => Some("png"),
            FileType::Rar => Some("rar"),
            FileType::Rlib => Some("rlib"),
            FileType::Rm => Some("rm"),
            FileType::Rmvb => Some("rmvb"),
            FileType::Sevenzip => Some("7z"),
            FileType::Svg => Some("svg"),
            FileType::Text => Some("txt"),
            FileType::Toml => Some("toml"),
            FileType::Torrent => Some("torrent"),
            FileType::Unknown(_) => Some("unknown"),
            FileType::Vid => Some("vid"),
            FileType::Vtx => Some("vtx"),
            FileType::Webm => Some("webm"),
            FileType::Webp => Some("webp"),
            FileType::Wmv => Some("wmv"),
            FileType::Xcf => Some("xcf"),
            FileType::Yaml => Some("yaml"),
            FileType::Zip => Some("zip"),
        }
    }
}

#[derive(Debug, Default)]
pub enum FileCategory {
    Archive,
    Document,
    Image,
    Other,
    #[default]
    Unknown,
    Video,
}

impl From<FileCategory> for &str {
    fn from(file_category: FileCategory) -> Self {
        match file_category {
            FileCategory::Archive => "archives",
            FileCategory::Document => "documents",
            FileCategory::Image => "images",
            FileCategory::Other => "other",
            FileCategory::Unknown => "unknown",
            FileCategory::Video => "videos",
        }
    }
}

impl From<&FileType> for FileCategory {
    fn from(file_type: &FileType) -> Self {
        match file_type {
            FileType::Unknown(_) => FileCategory::Unknown,
            FileType::Png
            | FileType::Jpg
            | FileType::Webp
            | FileType::Jfif
            | FileType::Gif
            | FileType::Svg => FileCategory::Image,
            FileType::Avi
            | FileType::Mp4
            | FileType::Mkv
            | FileType::Wmv
            | FileType::Webm
            | FileType::Asf
            | FileType::Flv
            | FileType::M4v
            | FileType::Mov
            | FileType::Mpg
            | FileType::Rm
            | FileType::Rmvb
            | FileType::Vid => FileCategory::Video,
            FileType::Toml
            | FileType::Json
            | FileType::Yaml
            | FileType::Markdown
            | FileType::Text
            | FileType::Csv
            | FileType::Pdf
            | FileType::Odt => FileCategory::Document,
            FileType::Xcf
            | FileType::Php
            | FileType::Torrent
            | FileType::Html
            | FileType::Vtx
            | FileType::Db => FileCategory::Other,
            FileType::Rlib | FileType::Rar | FileType::Zip | FileType::Sevenzip => {
                FileCategory::Archive
            }
        }
    }
}

impl TryFrom<&Path> for FileInfo {
    type Error = FileInfoError;

    fn try_from(path: &Path) -> Result<Self, Self::Error> {
        if !path.exists() {
            return Err(FileInfoError::PathDoesNotExist(path.to_owned()));
        }
        if !path.is_file() {
            return Err(FileInfoError::PathIsNotAFile(path.to_owned()));
        }
        let mut file_info = FileInfo::default();
        file_info.path = path.to_owned();
        file_info.r#type = detect_filetype(path);
        file_info.category = FileCategory::from(&file_info.r#type);
        Ok(file_info)
    }
}

impl FileInfo {
    pub async fn hash(&self) -> Result<String, FileInfoError> {
        file_hash_from_path(&self.path)
    }
}

pub fn file_hash_from_path(path: &Path) -> Result<String, FileInfoError> {
    use sha2::Digest;

    let mut hasher = sha2::Sha512::default();
    let mut file = std::fs::File::open(path)?;
    let _ = std::io::copy(&mut file, &mut hasher)?;
    drop(file);
    let hash_bytes = hasher.finalize().as_slice().to_vec();
    let hash = base16ct::lower::encode_string(&hash_bytes);
    Ok(hash)
}

fn detect_filetype(path: &Path) -> FileType {
    if let Some(extension) = path.extension() {
        let extension = extension.to_string_lossy().to_lowercase();
        return match extension.as_str() {
            "7z" => FileType::Sevenzip,
            "asf" => FileType::Asf,
            "avi" => FileType::Avi,
            "csv" => FileType::Rar,
            "db" => FileType::Db,
            "flv" => FileType::Flv,
            "gif" => FileType::Gif,
            "htm" | "html" => FileType::Html,
            "jfif" => FileType::Jfif,
            "jpg" | "jpeg" => FileType::Jpg,
            "json" => FileType::Json,
            "m4v" => FileType::M4v,
            "md" => FileType::Markdown,
            "mkv" => FileType::Mkv,
            "mov" => FileType::Mov,
            "mp4" => FileType::Mp4,
            "mpg" | "mpeg" => FileType::Mpg,
            "odt" => FileType::Odt,
            "pdf" => FileType::Pdf,
            "php" => FileType::Php,
            "png" => FileType::Png,
            "rar" => FileType::Rar,
            "rlib" => FileType::Rlib,
            "rm" => FileType::Rm,
            "rmvb" => FileType::Rmvb,
            "svg" => FileType::Svg,
            "toml" => FileType::Toml,
            "torrent" => FileType::Torrent,
            "txt" | "d" => FileType::Text,
            "vid" => FileType::Vid,
            "vtx" => FileType::Vtx,
            "webm" => FileType::Webm,
            "webp" => FileType::Webp,
            "wmv" => FileType::Wmv,
            "xcf" => FileType::Xcf,
            "yml" | "yaml" => FileType::Yaml,
            "zip" => FileType::Zip,
            any => FileType::Unknown(Some(any.to_owned())),
        };
    }
    tracing::debug!("can't detect filetype from extension: {:?}", path);
    FileType::Unknown(None)
}
